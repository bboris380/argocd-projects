# Guide for RDS access
This is a short guide how to use connect to RDS frok EKS pod.

## Requirements
Install kubectl [documentation](https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/).<br />
Install aws-cli [documentation](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).<br />
Configure keys [documentation](https://docs.aws.amazon.com/toolkit-for-visual-studio/latest/user-guide/keys-profiles-credentials.html).<br />
Export kube-config for EKS cluster [documentation](https://aws.amazon.com/premiumsupport/knowledge-center/eks-generate-kubeconfig-file-for-cluster/).<br />
Lens installation url [documentation](https://k8slens.dev/).<br />
 
## How to access RDS:
Get RDS credentials from EKS secret via command or use lens: `kubectl get secret -n app sparx-secret -o json | jq '.data | map_values(@base64d)'` <br />
Run ubuntu shell pod: `kubectl run -i --tty --rm -n app --image ubuntu:20.04 shell /bin/bash`<br />
Install mysql-client and other tools if needed: `apt update && apt install -y mysql-client`<br />
Then connect to mysql-host using vars from secret: `mysql -u DB_USERNAME -p -h DB_HOSTNAME`<br />


# Guide for suspender
This is a short guide how to use suspender feature.

## Guide how to suspend/unsuspend applcation in EKS:
To suspend app go to eg: https://stage.sparx.ai/suspend and click on button<br />
To unsuspend app go to eg: https://stage.sparx.ai/unsuspend and click on button<br />
